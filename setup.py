import setuptools

with open('README.md', 'r') as file:
    long_description = file.read()

setuptools.setup(
    name='xml2json',
    version='0.1.0',
    scripts=['xml2json'],
    author="Bogdan Rizac",
    author_email="367815-marius-rizac@users.noreply.gitlab.com",
    description="A simple XML to JSON CLI convertor",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kisphp/python-xml2json.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Pyrhon :: 3",
        "Licence :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ]
)
